// // // // // // // // // // // // // // // // // // // // // // // // // // // 
//ZeroINOX-1.0 - 3D Agent forked from Armanda-agent (https://patriz.io/agent/)//
// // // // // // // // // // // // // // // // // // // // // // // // // // // 
//                           di Daniele Di Ottavio                            //
// // // // // // // // // // // // // // // // // // // // // // // // // // // 

// // // // // // // // // // // // // // // // // // // // // // // // // // //

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// GLOBAL

let startingTimer = null;
let avatarReady = false;

let startAudioBut = null;
let stopAudioBut = null;
let speakerTestBut = null;
let startMicBut = null;
let stopMicBut = null;
let startCamBut = null;
let stopCamBut = null;

let asrState = null;

let phonemesCombo = null;
let loadLabialBut = null;
let saveLabialBut = null;
let resetLabialBut = null;

let proto = "";
let port = 10000;

let notifier = null;
let aCtx = null;
let source = null;
let audioStarted = false;

const rate = 22050;
const bufferSize = 2048;
const numberOfInputChannels = 1;  // Input mono
const numberOfOutputChannels = 1;  // Output mono
let microphoneInput = null;
let microphoneStream = null;
let scriptNode = null;
const micHeaderBlob = new Blob([(new TextEncoder()).encode(' MIC')]);

let videoElement = null;
let videoOutputCanvas = null;
let videoOutputCtx = null;
let videoPreviewCanvas = null;
let videoPreviewCtx = null;
let videoStream = null;
let fps = 15;
let fpsTimerID = null;
let jpegQuality = 0.7;
const camHeaderBlob = new Blob([(new TextEncoder()).encode(' CAM')]);

let lastMotionArea = null;
let lastMotionBoxes = null;
let lastFacesBoxes = null;
let lastCodesMeshes = null;
//let watchTo = null;
let lastWatchingX = 0;
let lastWatchingY = 0;
let blinking = false;

let defaultAvatarFace = {
    blendJawRange : 0,
    blendSyncRange : 50,
    blendTwistRange : 50,
    blendLipRange : 50,
    blendBlinkRange : 60,
    blendWidthRange : 50,
    blendExpressRange : 50,
    blendSymRange : 0,
    blendTongueRange : 50,
    blendMaleRange : 0
};

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// DATA STRUCTUREs

/*class Queue
{
    queue = [];

    constructor(list)
    {
        if (list)
            this.queue = list;
    }

    enqueue(item)
    {
        this.queue.push(item);
    }

    dequeue()
    {
        if (this.isEmpty())
            return null;
        
        return this.queue.shift();
    }

    top()
    {
        if (this.isEmpty())
            return null;

        return this.queue[0];
    }

    isEmpty()
    {
        return (this.queue.length === 0);
    }

    count()
    {
        return this.queue.length;
    }
}*/

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// PLAYBACK

function playPCM(blob)
{
    console.log("Playing PCM chunk [" + blob.size + " B]")
    
    const reader = new FileReader();
  
    reader.onload = async function(event)
    {
        const arrayBuffer = event.target.result;
    
        const format = {
            sampleRate: 22050,
            numberOfChannels: 1,
            signed: true,
            bitDepth: 16
        };

        const audioBuffer = aCtx.createBuffer(
            format.numberOfChannels,
            arrayBuffer.byteLength / (format.bitDepth / 8),
            format.sampleRate);
        
        const channelData = audioBuffer.getChannelData(0);

        //!!!
        //would be
        //const channelData = int16ToFloat32(audioBuffer.getChannelData(0))
        
        const dataView = new DataView(arrayBuffer);
        
        //dirty patch
        //!!!
        for (let i = 0; i < channelData.length; i++)
            channelData[i] = dataView.getInt16(i * 2, true) / 32768;
        //!!!
        
        source = aCtx.createBufferSource();
        source.buffer = audioBuffer;

        source.connect(aCtx.destination);

        source.addEventListener('ended', function ()
        {
            sendEvent("SPEECH_FINISHED", null);

            setTimeout(function()
            {
                if (aCtx != null)
                    speakerTestBut.disabled = false;
                
            }, 500);
                
        });

        source.start();
        sendEvent("SPEECH_STARTED", null);
    };
  
    reader.readAsArrayBuffer(blob);
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// SPEAKER

function speakerTest()
{
    sendEvent("SPKR_TEST", null);
    speakerTestBut.disabled = true;
    console.log("Speaker test REQUESTED");
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// AUDIO ENGINE

function startAudio()
{
    if (aCtx)
    {
        console.error("Audio engine is ALREADY started!");
        return;
    }

    aCtx = new (window.AudioContext || window.webkitAudioContext)({sampleRate: rate});
    audioStarted = true;

    startAudioBut.disabled = true;
    stopAudioBut.disabled = false;

    speakerTestBut.disabled = false;

    startMicBut.disabled = false;

    sendEvent("AUDIO_ON", null);
    console.log("Audio engine STARTED");
}

function stopAudio()
{
    if (aCtx == null)
    {
        console.error("Audio engine is NOT started yet!");
        return;
    }

    if (source)
    {
        source.stop();
        console.log("Playback STOPPED");
    }

    if (microphoneStream)
        stopMic();

    startMicBut.disabled = true;

    aCtx = null;
    audioStarted = false;

    startAudioBut.disabled = false;
    stopAudioBut.disabled = true;

    speakerTestBut.disabled = true;

    sendEvent("AUDIO_OFF", null);
    console.log("Audio engine STOPPED");
}

function int16ToFloat32(intArray)
{
    const floatArray = new Float32Array(intArray.length);

    for (let i = 0; i < intArray.length; i++) {
      const intSample = intArray[i];
      const floatValue = intSample / 32767;
      floatArray[i] = floatValue;
    }

    return floatArray;
}

function float32ToInt16(floatArray) 
{
    const intArray = new Int16Array(floatArray.length);

    for (let i = 0; i < floatArray.length; i++)
    {
        const floatValue = floatArray[i];
        const intSample = Math.floor(floatValue * 32767);
        intArray[i] = intSample;
    }

    return intArray;
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// MIC

function sendMicPCM(pcm)
{
    const audioBuffer = pcm;
    const audioData = audioBuffer.getChannelData(0);
    const audioBlob = new Blob([float32ToInt16(audioData)], {type: 'audio/wav'});
    //console.log(audioBlob.size);
    
    if (!notifier.isConnected())
        return;
    
    notifier.send(new Blob([micHeaderBlob, audioBlob]))
}

function startMic()
{
    if (aCtx == null)
    {
        console.error("Audio engine NOT started yet!");
        return;
    }

    if (microphoneStream)
    {
        console.error("Microphone is ALREADY capturing!");
        return;
    }

    startMicBut.disabled = true;
    stopMicBut.disabled = false;

    navigator.mediaDevices
        .getUserMedia({audio: true})
        .then(function (stream) {
            microphoneStream = stream;
            microphoneInput = aCtx.createMediaStreamSource(microphoneStream);
            scriptNode = aCtx.createScriptProcessor(bufferSize, numberOfInputChannels, numberOfOutputChannels);
            scriptNode.onaudioprocess = function (event) {sendMicPCM(event.inputBuffer);};
            microphoneInput.connect(scriptNode);
            scriptNode.connect(aCtx.destination);
        })
        .catch(function (error) {
            console.error("Error obtaining microphone:", error);
        });


    sendEvent("MIC_ON", null);
    console.log("Microphone capture STARTED");
}

function stopMic()
{
    if (microphoneStream == null)
    {
        console.error("Microphone is NOT capturing yet!");
        return;
    }

    startMicBut.disabled = false;
    stopMicBut.disabled = true;

    const audioTracks = microphoneStream.getAudioTracks();
    audioTracks.forEach(function (track) {track.stop();});

    microphoneInput.disconnect();
    
    scriptNode.disconnect();
    scriptNode.onaudioprocess = null;
    scriptNode = null

    microphoneStream = null;
    sendEvent("MIC_OFF", null);
    console.log("Microphone capture STOPPED");
    asrState.style.backgroundColor = "red";
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// CAM

function drawDetectionBoxes(boxes, color, thickness)
{
    for(const box of boxes)
    {
        videoOutputCtx.strokeStyle = color;
        videoOutputCtx.lineWidth = thickness;
        
        videoOutputCtx.strokeRect(
            box.x,
            box.y, 
            box.w, 
            box.h);
    }
}

function drawDetectionCodeMeshes()
{
    for(const mesh of lastCodesMeshes)
    {
        videoOutputCtx.beginPath();
        let polygon = mesh.polygon;

        videoOutputCtx.moveTo(polygon[0].x, polygon[0].y);

        for (var i=1; i<polygon.length; i++) 
            videoOutputCtx.lineTo(polygon[i].x, polygon[i].y);

        videoOutputCtx.closePath();

        videoOutputCtx.strokeStyle = "magenta";
        videoOutputCtx.lineWidth = 10;
        videoOutputCtx.stroke();
    }
}

function drawViewFinder(x, y)
{
    tempX = x;

    videoOutputCtx.strokeStyle = "red";
    videoOutputCtx.lineWidth = 6;

    videoOutputCtx.beginPath();
    videoOutputCtx.moveTo(tempX-20, y);
    videoOutputCtx.lineTo(tempX+20, y);
    videoOutputCtx.closePath();
    videoOutputCtx.stroke();
    
    videoOutputCtx.beginPath();
    videoOutputCtx.moveTo(tempX, y-20);
    videoOutputCtx.lineTo(tempX, y+20);
    videoOutputCtx.closePath();
    videoOutputCtx.stroke();
}

function canvasToBlobWithQuality(canvas) {
    return new Promise((resolve) => {
        canvas.width = videoElement.videoWidth;
        canvas.height = videoElement.videoHeight;

        videoOutputCtx.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
        canvas.toBlob((blob) => {
            resolve(blob);
        }, 'image/jpeg', jpegQuality);
    });
}

function sendFrame()
{    
    canvasToBlobWithQuality(videoOutputCanvas)
        .then((frameBlob) => {
            if (frameBlob == null)
                return;

            if (notifier.isConnected())
                notifier.send(new Blob([camHeaderBlob, frameBlob]));
            
            if (lastMotionArea)
                drawDetectionBoxes([lastMotionArea], 'white', 5);
                
            if (lastMotionBoxes)
                drawDetectionBoxes(lastMotionBoxes, 'yellow', 2);

            if (lastFacesBoxes)
                drawDetectionBoxes(lastFacesBoxes, 'lightgreen', 5);

            if (lastCodesMeshes)
                drawDetectionCodeMeshes();

            drawViewFinder(lastWatchingX, lastWatchingY);
            videoPreviewCtx.drawImage(videoOutputCanvas, 0, 0, videoPreviewCanvas.width, videoPreviewCanvas.height);
        })
        .catch((error) => {
            console.error('Blob conversion error:', error);
        });
}

function startCam()
{
    if (videoElement)
    {
        console.error("Camera is ALREADY capturing!");
        return;
    }

    startCamBut.disabled = true;
    stopCamBut.disabled = false;

    videoElement = document.getElementById('video');
    videoOutputCanvas = document.getElementById('sendingFrame');
    videoOutputCtx = videoOutputCanvas.getContext('2d');
    
    videoPreviewCanvas = document.getElementById('preview');
    videoPreviewCtx = videoPreviewCanvas.getContext('2d');
    videoPreviewCanvas.style.display = "block";
    
    navigator.mediaDevices.getUserMedia({video: {width: 640, height: 360}})
            .then(function(stream) {
                videoStream = stream;
                videoElement.srcObject = videoStream;
                fpsTimerID = setInterval(function() {
                    sendFrame();
                }, 1000/fps);

                sendEvent("CAM_ON", null);
            })
            .catch(function(error) {
                console.error("Error obtaining camera:", error);
            });
}

function stopCam()
{
    if (videoElement == null)
    {
        console.error("Camera is NOT capturing yet!");
        return;
    }

    videoPreviewCanvas.style.display = "none";

    startCamBut.disabled = false;
    stopCamBut.disabled = true;

    const videoTracks = videoStream.getTracks();
    videoTracks.forEach(function (track) {track.stop();});

    clearInterval(fpsTimerID);
    videoElement.srcObject = null;
    videoElement = null;
    videoOutputCanvas = null;
    videoOutputCtx = null;
    videoPreviewCanvas = null;
    videoPreviewCtx = null;
    videoStream = null;
    fpsTimerID = null;

    sendEvent("CAM_OFF", null);
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// WEBSOCKET

function buildWs(url, onmessage)
{
    notifier = new WebSocket(url);
    console.log("Connecting to:", url)

    notifier.onopen = function()
    {
        console.log(url, "Connected");
        readyCB = onAvatarReady;
        init();
    };

    notifier.onclose = function()
    {
        if (avatarReady)
            resetAvatarFace();

        if (videoElement)
            stopCam();

        if (microphoneStream)
            stopMic();

        if (aCtx)
            stopAudio();

        notifier = null;
        console.log(url, "Disconnected");
        alert("Agent is disconnected");
    }

    notifier.onerror = function(event)
    {
        //document.getElementById("loading");
        console.error("WebSocket Error");
        alert("WebSocket Error.");
    };

    notifier.isConnected = function()
    {
        return (notifier.readyState === WebSocket.OPEN);
    }

    notifier.onmessage = onmessage;
}

function onReadyRead(evt)
{
    if (!avatarReady)
        return;
    
    if (typeof evt.data === 'string')
    {
        var m = JSON.parse(evt.data);
        var cmd = m["cmd"];

        if (cmd == "DETECTION")
        {
            let pck = m["data"];

            if (pck.hasOwnProperty("motionArea"))
                lastMotionArea = pck.motionArea;

            if (pck.hasOwnProperty("motionBlocks"))
                lastMotionBoxes = pck.motionBlocks;
            
            if (pck.hasOwnProperty("faces"))
                lastFacesBoxes = pck.faces;

            if (pck.hasOwnProperty("codes"))
                lastCodesMeshes = pck.codes;
        }

        else if (cmd == "DETECTION_RESET")
        {
            lastMotionArea = null;
            lastMotionBoxes = null;
            lastFacesBoxes = null;
            lastCodesMeshes = null;
        }
        
        else if (cmd == "WATCH_TO")
        {
            let watchTo = m["data"];
            lastWatchingX = watchTo[0];
            lastWatchingY = watchTo[1];
            setWatchingDirection(lastWatchingX, lastWatchingY);
        }

        else if (cmd == "EYE_BLINK")
            eyesBlink();

        else if (cmd == "SET_KNOWN_PHONEMES")
        {
            let phonemes = m["data"];
            setLabialsCombo(phonemes);
        }

        else if (cmd == "SET_CURRENT_LABIAL")
        {
            let currentLabial = m["data"];            
            loadPhonemeLabial(currentLabial);
        }

        else if (cmd == "ASR_IDLE")
        {
            asrState.style.backgroundColor = "green";
            console.log("ASR_IDLE");
        }

        else if (cmd == "ASR_CAPTURING")
        {
            asrState.style.backgroundColor = "orange";
            console.log("ASR_CAPTURING");
        }
        
        else if (cmd == "ASR_BUSY")
        {
            asrState.style.backgroundColor = "red";
            console.log("ASR_BUSY");
        }

        else if (cmd == "RESET_CURRENT_LABIAL")
            resetAvatarFace();

        else
            console.log("Command UNKNOWN -", cmd);
    }

    else if (audioStarted === true)
        playPCM(evt.data);
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// PROGRAM

$(document).ready(function ()
{
    window.addEventListener('focus', function() {
        if (document.hasFocus()) sendEvent("GOTTEN_FOCUS", null);
      });
      
    window.addEventListener('blur', function() {
        if (!document.hasFocus()) sendEvent("LOST_FOCUS", null);
      });

    document.addEventListener("keydown", onDocumentKeyDown, false)

    startAudioBut = document.getElementById('startAudioBut');
    startAudioBut.addEventListener('click', startAudio);
    startAudioBut.disabled = false;

    stopAudioBut = document.getElementById('stopAudioBut');
    stopAudioBut.addEventListener('click', stopAudio);
    stopAudioBut.disabled = true;

    speakerTestBut = document.getElementById('speakerTestBut');
    speakerTestBut.addEventListener('click', speakerTest);
    speakerTestBut.disabled = true;

    startMicBut = document.getElementById('startMicBut');
    startMicBut.addEventListener('click', startMic);
    startMicBut.disabled = true;

    stopMicBut = document.getElementById('stopMicBut');
    stopMicBut.addEventListener('click', stopMic);
    stopMicBut.disabled = true;

    startCamBut = document.getElementById('startCamBut');
    startCamBut.addEventListener('click', startCam);
    startCamBut.disabled = false;

    stopCamBut = document.getElementById('stopCamBut');
    stopCamBut.addEventListener('click', stopCam);
    stopCamBut.disabled = true;

    asrState = document.getElementById('asrState');
    asrState.style.backgroundColor = "red";

    initSlider("blendJawRange", 0, blendJaw);
    initSlider("blendTwistRange", 50, blendTwist);
    initSlider("blendLipRange", 50, blendLip);
    initSlider("blendBlinkRange", 60, blendBlink);
    initSlider("blendWidthRange", 50, blendWidth);
    initSlider("blendExpressRange", 50, blendExpress);
    initSlider("blendSymRange", 0, blendSym);
    initSlider("blendTongueRange", 50, blendTongue);
    initSlider("blendMaleRange", 0, blendMale);
    initSlider("blendSyncRange", 50,
        function (value) {
            let expressval = $("#blendExpressRange").slider("value");
            blendSync(value, expressval);
        });

    phonemesCombo = document.getElementById('phonemes');
    phonemesCombo.addEventListener('change', onPhonemeSelected);

    loadLabialBut = document.getElementById('loadLabialBut');
    loadLabialBut.addEventListener('click', requestPhonemeLabial);
    loadLabialBut.disabled = true;

    saveLabialBut = document.getElementById('saveLabialBut');
    saveLabialBut.addEventListener('click', savePhonemeLabial);
    saveLabialBut.disabled = true;

    resetLabialBut = document.getElementById('resetLabialBut');
    resetLabialBut.addEventListener('click', resetAvatarFace);

    if ("WebSocket" in window)
    {
        if (document.location.protocol === 'https:')
            proto = "wss://";

        else
            proto = "ws://";

        buildWs(proto + location.hostname + ":" + port + "/agent", onReadyRead);
    }

    else
        alert('WebSocket NOT supported by your Browser!');
});

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// INIT

function onAvatarReady()
{
    let d = document.getElementById("loading");
    d.style.display = "none";
    renderReady = 0;
    avatarReady = true;
    resetAvatarFace();        
    
    sendEvent("READY", null);
    document.getElementById('zeroAvatar').style.display = "block";
    //startAudio();
    //startCam();
}

function initSlider(name, value, cb)
{
    $("#" + name).slider({
        range: "max",
        min: 0,
        max: 100,
        value: value,
        slide: function (event, ui)
        {
            cb(ui.value);
            render();
        }
    });
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// EYEs

function setWatchingDirection(x, y)
{
    x = videoOutputCanvas.width-x; 

    x -= (camera.target.position.x - camera.position.x + groupHead.rotation.x);
    y += (camera.target.position.y - camera.position.y + groupHead.rotation.y);

    x = (w/4)+(x/2);
    y = (h/4)+(y/2);

    x = (x / w) * 2 - 1;
    y = -(y / h) * 2 + 1;

    const factor = 2.5;
    mesh.morphTargetInfluences[4] = -x * factor;
    mesh.morphTargetInfluences[5] = x * factor;
    mesh.morphTargetInfluences[6] = (y * factor) * eyelock;
    mesh.morphTargetInfluences[7] = -(y * factor) * eyelock;

    groupLeye.rotation.y = (x * 1) / 2;
    groupReye.rotation.y = (x * 1) / 2;
    groupLeye.rotation.x = -y / 2;
    groupReye.rotation.x = -y / 2;
    groupHead.rotation.y = x / 8;
    groupHead.rotation.x = -y / 8;

    render();
}

function eyesBlink()
{
    if (blinking)
        return;
    
    //console.log("EYE_BLINK");
    let originalblink = defaultAvatarFace.blendBlinkRange;
    
    blendBlink(100);
    $("#blendBlinkRange").slider("value", 100);
    render();

    blinking = true;

    setTimeout(function()
    {
        let currVal = originalblink+10;
        blendBlink(currVal);
        $("#blendBlinkRange").slider("value", currVal);
        render();

        setTimeout(function()
        {
            blendBlink(originalblink);
            $("#blendBlinkRange").slider("value", originalblink);
            render();
            blinking = false;
        }, 120);
    }, 230);
}

function onDocumentKeyDown(event)
{
    switch (event.keyCode) {
    case 37:
        camera.position.x += 8;
        break;
    case 38:
        camera.position.y += 8;
        break;
    case 39:
        camera.position.x -= 8;
        break;
    case 40:
        camera.position.y -= 8;
        break
    }

    console.log("DOWN", event.keyCode);

    render();
    event.preventDefault();
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// REQUEST

function sendEvent(evt_T, args)
{
    if (!notifier.isConnected())
        return

    let evt = {};
    evt.type = evt_T;

    if (args !== null)
        evt.args = args;

    const json = JSON.stringify(evt);
    notifier.send(json);
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 
// LABIALs AND PHONEMEs

function setLabialsCombo(phonemes)
{
    //console.log(phonemes);
    phonemesCombo.innerHTML = "";

    let option = document.createElement('option');
    option.value = "-1";
    option.text = "Seleziona il phonema";
    phonemesCombo.appendChild(option);

    for (const ph of phonemes)
    {
        option = document.createElement('option');
        option.value = ph.phoneme;

        if (ph.valid == true)
            option.text = "Phonema: '" + ph.phoneme + "' - ok";
        else
            option.text = "Phonema: '" + ph.phoneme + "' - non impostato";

        phonemesCombo.appendChild(option);
    }
}

function onPhonemeSelected()
{
    let selectedValue = phonemesCombo.value;
    loadLabialBut.disabled = (selectedValue == -1);
    saveLabialBut.disabled = (selectedValue == -1);
    console.log('Opzione selezionata:', selectedValue);
}

function requestPhonemeLabial()
{
    let selectedValue = phonemesCombo.value;
    sendEvent("LOAD_PHONEME_LABIAL", selectedValue);
}

function loadPhonemeLabial(parameters)
{
    if (!parameters.valid)
        return;

    $("#blendJawRange").slider("value", parameters.blendJawRange);
    blendJaw(parameters.blendJawRange);

    $("#blendTwistRange").slider("value", parameters.blendTwistRange);
    blendTwist(parameters.blendTwistRange);

    $("#blendLipRange").slider("value", parameters.blendLipRange);
    blendLip(parameters.blendLipRange);

    if (parameters.hasOwnProperty("blendBlinkRange"))
    {
        $("#blendBlinkRange").slider("value", parameters.blendBlinkRange);
        blendBlink(parameters.blendBlinkRange);
    }

    $("#blendWidthRange").slider("value", parameters.blendWidthRange);
    blendWidth(parameters.blendWidthRange);    
    
    $("#blendExpressRange").slider("value", parameters.blendExpressRange);
    blendExpress(parameters.blendExpressRange);
    
    $("#blendSymRange").slider("value", parameters.blendSymRange);
    blendSym(parameters.blendSymRange);
    
    $("#blendTongueRange").slider("value", parameters.blendTongueRange);
    blendTongue(parameters.blendTongueRange);
    
    $("#blendSyncRange").slider("value", parameters.blendSyncRange);
    blendSync(parameters.blendSyncRange, $("#blendExpressRange").slider("value"));

    if (parameters.hasOwnProperty("blendMaleRange"))
    {
        $("#blendMaleRange").slider("value", parameters.blendMaleRange);
        blendMale(parameters.blendMaleRange);
    }

    render();
}

function savePhonemeLabial()
{
    var labial = {};
    labial.phoneme = phonemesCombo.value;
    labial.blendJawRange = $("#blendJawRange").slider("value");
    labial.blendSyncRange = $("#blendSyncRange").slider("value");
    labial.blendTwistRange = $("#blendTwistRange").slider("value");
    labial.blendLipRange = $("#blendLipRange").slider("value");
    //labial.blendBlinkRange = $("#blendBlinkRange").slider("value");
    labial.blendWidthRange = $("#blendWidthRange").slider("value");
    labial.blendExpressRange = $("#blendExpressRange").slider("value");
    labial.blendSymRange = $("#blendSymRange").slider("value");
    labial.blendTongueRange = $("#blendTongueRange").slider("value");
    //labial.blendMaleRange = $("#blendMaleRange").slider("value");

    sendEvent("SAVE_PHONEME_LABIAL", labial);
    saveLabialBut.disabled = true;
    //select first
}

function resetAvatarFace()
{
    blendJaw(defaultAvatarFace.blendJawRange);
    blendTwist(defaultAvatarFace.blendTwistRange);
    blendLip(defaultAvatarFace.blendLipRange);
    blendBlink(defaultAvatarFace.blendBlinkRange);
    blendWidth(defaultAvatarFace.blendWidthRange);
    blendExpress(defaultAvatarFace.blendExpressRange);
    blendSym(defaultAvatarFace.blendSymRange);
    blendTongue(defaultAvatarFace.blendTongueRange);
    blendMale(defaultAvatarFace.blendMaleRange);
    blendSync(defaultAvatarFace.blendSyncRange, defaultAvatarFace.blendExpressRange);

    render();
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // 